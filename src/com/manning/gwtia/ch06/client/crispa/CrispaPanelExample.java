package com.manning.gwtia.ch06.client.crispa;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.i18n.client.Constants;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;



public class CrispaPanelExample extends Composite
{
    @UiField (provided =true)
    FlexTable table;
    
    /**
     * The main data grid
     */
    @UiField(provided =true)
    CellTable<Experiment> expCellTable;
    
    @UiField(provided =true)
    SimplePager pager;
        
    Logger logger = Logger.getLogger("CrispaPanel logging");
    
    Column<Experiment, String> testCol = null;
    
    Column<Experiment, Date> expCol = null;
    
    ListDataProvider<Experiment> experimentListDataProvider = null;
    
    private static List<TextColumn<Experiment>> columnObject = new ArrayList();
        
    private static CrispaPanelExampleUiBinder uiBinder = GWT.create(CrispaPanelExampleUiBinder.class);
    
    interface CrispaPanelExampleUiBinder extends UiBinder<Widget, CrispaPanelExample> {}


    public CrispaPanelExample()
    {
        createExeriment();
        loadTable();
        setUpCellTable();
        loadSimplePager();
        logger.log(Level.INFO, "Initalize CrispaPanel");
        initWidget(uiBinder.createAndBindUi(this));
    }
    
    public static interface CwConstants extends Constants {
        
        String cwDataGridColumnAnimalName();
        String cwDataGridColumnExperimentName();
        String cwDataGridColumnAssayId();
        String cwDataGridColumnExperimenter();
        String cwDataGridExperimentDate();
    }
        
    private void setUpCellTable()
    {
        expCellTable = new CellTable<>();
        expCellTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);        
        addColumn(expCellTable);
        makeTableColumnSortable(expCellTable);        
        expCellTable.setRowCount(EXPERIMENT.size(), true);
    }
    
    private void addColumn(CellTable<Experiment> expCellTable)
    {
        // 
       
        
        TextColumn<Experiment> idColumn = new TextColumn<Experiment>()
        {

            @Override
            public String getValue(Experiment object)
            {
                // TODO Auto-generated method stub
                return object.id + "";
            }
        };
        columnObject.add(idColumn);
        
        
        expCellTable.addColumn(idColumn, "Id");
        
        Column<Experiment, String> animalnameCol = new Column<Experiment, String>(
                new EditTextCell()) {
              @Override
              public String getValue(Experiment aNimal) {
                return aNimal.animalName;
              }
            };

            testCol = animalnameCol;
            expCellTable.addColumn(animalnameCol, "Animal Name");
            columnObject.add(null);
        
/*        TextColumn<Experiment> animalNameColumn = new TextColumn<Experiment>()
        {

            @Override
            public String getValue(Experiment object)
            {
                // TODO Auto-generated method stub
                return object.animalName;
            }
        };
        columnObject.add(animalNameColumn);
        expCellTable.addColumn(animalNameColumn, "Animal Name");*/
            
            
            
       
        
        TextColumn<Experiment> assyaNameColumn = new TextColumn<Experiment>()
        {

            @Override
            public String getValue(Experiment object)
            {
                // TODO Auto-generated method stub
                return object.assayName;
            }
        };
        columnObject.add(assyaNameColumn);
        expCellTable.addColumn(assyaNameColumn, "Assay Name");
        
        
        TextColumn<Experiment> assayIdColumn = new TextColumn<Experiment>()
        {

            @Override
            public String getValue(Experiment object)
            {
                // TODO Auto-generated method stub
                return object.assayId;
            }
        };
        
        columnObject.add(assayIdColumn);
        expCellTable.addColumn(assayIdColumn, "Assay Id");
        
        
        TextColumn<Experiment> experimenterColumn = new TextColumn<Experiment>()
        {

            @Override
            public String getValue(Experiment object)
            {
                // TODO Auto-generated method stub
                return object.experimenter;
            }
        };
        columnObject.add(experimenterColumn);
        expCellTable.addColumn(experimenterColumn, "Experimenter");
        
/*        TextColumn<Experiment> experimentDateColumn = new TextColumn<Experiment>()
        {

            @Override
            public String getValue(Experiment object)
            {
               
                DateTimeFormat dt = DateTimeFormat.getFormat("yyyy/MM/dd");
                return dt.format(object.experimentDate);
                
            }
        };
        columnObject.add(experimentDateColumn);
        expCellTable.addColumn(experimentDateColumn, "Experiment Date");
        */
        
        // Date of experiment editable cell table
        Column<Experiment, Date>  expDateCol= new Column<Experiment, Date>(new DatePickerCell()){

            @Override
            public Date getValue(Experiment exp)
            {
/*                DateTimeFormat dt = DateTimeFormat.getFormat("yyyy/MM/dd");
                return dt.format(exp.experimentDate);*/
                return exp.experimentDate;
            }};
            
/*            expDateCol.setFieldUpdater(new FieldUpdater<Experiment, Date>()
            {

                @Override
                public void update(int index, Experiment object, Date value)
                {
                   //final Experiment expObj = getPresenter().edit();
                    
                    
                    
                }
            });    
            */
           
            columnObject.add(null);
            expCellTable.addColumn(expDateCol, "Experiment Date");
            expCol = expDateCol;
        
        
        
        
        
        
        
        
        
        
        
        
        
        // Adding remove row on celltable data
        
        Column<Experiment, String> deleteBtn = new Column<Experiment, String>(new ButtonCell()){

            @Override
            public String getValue(Experiment object)
            {
                // TODO Auto-generated method stub
                return "X";
            }};
            
      expCellTable.addColumn(deleteBtn);
      
      deleteBtn.setFieldUpdater(new FieldUpdater<Experiment, String>()
      {
        
            @Override
            public void update(int index, Experiment object, String value)
            {
                
                experimentListDataProvider.getList().remove(object);
                experimentListDataProvider.refresh();
                expCellTable.redraw();
            }
      });
      
    }
    
    private void loadTable()
    {
        table = new FlexTable();
        table.setWidget(0, 0, new Label("ID"));
        table.setWidget(0, 1, new Label("Experiment Date"));
        table.setWidget(0, 2, new Label("Name of Experiment"));
        table.setWidget(0, 3, new Label("Result"));
        logger.log(Level.INFO, "Crispa table creation");
    }
    
    private void makeTableColumnSortable(CellTable<Experiment> expCellTable)
    {
        experimentListDataProvider = new ListDataProvider<Experiment>();
        
        // connect the table to the data provider
        experimentListDataProvider.addDataDisplay(expCellTable);
        
        // Add the data to the data provider, which automatically pushes it to the widget
        List<Experiment> list = experimentListDataProvider.getList();
        
        EXPERIMENT.stream().forEach(item-> list.add(item));
        
        ListHandler<Experiment> columnSortHandler = new ListHandler<Experiment>(list);
        columnSortHandler.setComparator(columnObject.get(0), 
                (Experiment ex1, Experiment ex2) -> ex1.getId().compareTo(ex2.getId())
                );
        
        expCellTable.addColumnSortHandler(columnSortHandler);       
        expCellTable.getColumnSortList().push(columnObject.get(0));
        
        
        
        // By Animal Name sorting
        ListHandler<Experiment> animalCoumnSortHandler = new ListHandler<Experiment>(list);
        columnSortHandler.setComparator(testCol, 
                (Experiment ex1, Experiment ex2) -> ex1.animalName.compareTo(ex2.animalName));
        
        expCellTable.addColumnSortHandler(animalCoumnSortHandler);       
        expCellTable.getColumnSortList().push(testCol);
        
        
        // By Animal Name sorting
        ListHandler<Experiment> dateCoumnSortHandler = new ListHandler<Experiment>(list);
        columnSortHandler.setComparator(expCol, 
                (Experiment ex1, Experiment ex2) -> ex1.experimentDate.compareTo(ex2.experimentDate));
        
        expCellTable.addColumnSortHandler(dateCoumnSortHandler);       
        expCellTable.getColumnSortList().push(expCol);
                
        expCellTable.getColumn(0).setSortable(true);
        expCellTable.getColumn(1).setSortable(true);
        expCellTable.getColumn(5).setSortable(true);

        
    }
    
    public class Experiment
    {
        private Integer id;
        private String animalName;
        private String assayId;
        private String assayName;
        private String experimenter;
        private Date experimentDate;
        public Experiment(int id, String animalName, String assayId, String assayName, String experimenter)
        {
            super();
            this.id = id;
            this.animalName = animalName;
            this.assayId = assayId;
            this.assayName = assayName;
            this.experimenter = experimenter;
            this.experimentDate = new Date(generateDate());
            
        }
        public Integer getId()
        {
            return id;
        }
        public void setId(int id)
        {
            this.id = id;
        }
           
    }

    private List<Experiment> EXPERIMENT = new ArrayList();

    public  long generateDate()
    {
        GWT.log("logging date generation");
        Date dt = new Date();       
        long dts =dt.getTime();       
        dts = (long) (dts - (Math.random() * 1) * 1530888898 -  (Math.random() * 1) *  1502031298);       
        return dts;
    }
    
    private void loadSimplePager()
    {   
        SimplePager.Resources pagerResources = GWT.create(SimplePager.Resources.class);
        pager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0, true);
        pager.setDisplay(expCellTable);
        pager.setPageSize(30);               
    }
    
    private  void createExeriment()
    {
        String [] animalName = new String[] 
        {
                "B6NTAC-USA/774.1c",
                "CAB39L-TM1B-IC/8.1b",
                "FBXL21-TM2B-IC/12.1j",
                "PARD3-TM1B-IC/4.1a",
                "REXO2-TM1B-IC/4.1a",
                "GFI1B-TM1B-IC/9.1e",
                "ADAM23-TM1B-IC/7.1d",
                "VGF-DEL11-EM1-B6N-IC/1.2f",
                "NFIX-DEL20-EM1-B6N-IC/2.1a",
                "IRX3-DEL5-EM2-B6N-IC/9.2a"
        };
        
        String [] assayName = new String [] {
                "IMPC - Simplified IPGTT",
                "IMPC - Adult LacZ",
                "IMPC - Auditory Brain Stem Response",
                "IMPC - Eye (Slit Lamp and Opthalamoscope)",
                "IMPC - Body Composition - Dexa",
                "IMPC - Body Composition - Dexa",
                "IMPC - Calorimetry",
                "IMPC - ECHO",
                "IMPC - Histopathology",
                "Embroy - Histopathology"
        };
        
        String [] assayId = new String [] {
                "1011",
                "1012",
                "1013",
                "1014",
                "1015",
                "1016",
                "1017",
                "1018",
                "1019",
                "1020"
        };
        
        String [] experimenter = new String [] 
        {
                "Experimenter A",
                "Experimenter B",
                "Experimenter C",
                "Experimenter D",
                "Experimenter E",
                "Experimenter F",
                "Experimenter G",
                "Experimenter H",
                "Experimenter I",
                "Experimenter J"
        };
               
        Set<Date> dateSet = new HashSet<>();
        
        while(dateSet.size() != 10)
        {
            dateSet.add(new Date(generateDate()));
        }
                
        Set<Experiment> exp = new HashSet<>();
                
        Experiment ex = null;
        Random r1 = null;
        while(exp.size() != 500)
        {
            r1 = new Random();           
            ex = new Experiment(exp.size(), 
                    animalName[r1.nextInt(9)], 
                    assayId[r1.nextInt(9)], 
                    assayName[r1.nextInt(9)], 
                    experimenter[r1.nextInt(9)]);            
            exp.add(ex);
        }
        
        EXPERIMENT = new ArrayList<>(exp);                      
    }
}
