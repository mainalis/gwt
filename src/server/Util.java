package server;

import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;


public class Util
{
    public  static Date generateDate()
    {

        

        Date now = new Date();
        long sixMonthsAgo = (now.getTime() - 13885853500l);
        long today = now.getTime();


            long ms = ThreadLocalRandom.current().nextLong(sixMonthsAgo, today);
            return new Date(ms);


    }
}
